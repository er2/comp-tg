return {
  token = os.getenv 'TOKEN',
  owner = tonumber(os.getenv 'OWNER'),
  cmds = {
    'eval',
    'reload',
    'ping',
    'rub',
    'start',
    'shell',
  },
  events = {
    'command',
    'ready',
    'inlineQuery',
    'message',
  },
  parts = {
    'locale',
    'client',
  },
}
