-- Standard Library in bot

-- table.indexOf(table, value) - Get value of inverter value-key table.
function table.indexOf(t, val)
  local i = {}
  for k, v in pairs(t) do
    i[v] = k
  end
  return i[val]
end

-- table.find(table, value) - Return index if value matches.
function table.find(t, val)
  for k, v in pairs(t) do
    if v == val then
      return k
    end
  end
end

-- table.findV(table, value) - return value in table matching value table
function table.findV(t, val)
  local b = nil
  for _, v in pairs(t) do
    for k, x in pairs(val) do
      if x ~= v[k] then
        b = 1
        break
      end
    end
    if b then
      b = nil
    else
      return v
    end
  end
end

-- string.escp(string) - Escape reserved regex values
function string.escp(s)
  return s:gsub('[%^%$%%%(%)%.%[%]%*%+%-%?]', '%%%0')
end

-- dump(table, depth?) - Return string that shows table contents.
function dump(t, d)
  if not tonumber(d) or d < 0 then
    d = 0
  end
  local c = ''
  for k, v in pairs(t) do
    if type(v) == 'table' then
      v = '\n' .. dump(v, d + 1)
    elseif type(v) == 'userdata' then
      v = '<USERDATA>'
    end
    c = c .. ('%s%s = %s\n'):format((' '):rep(d), k, v)
  end
  return c
end

return function(C, api)
  C:load 'cmds'

  for _, lang in pairs(C.locale.list) do
    local a = {}
    for k, v in pairs(C.cmds) do
      if not (v.private or v.hide) then
        local cmd = C.locale:get('cmds', k, lang or {})
        table.insert(a, {
          command = k,
          description = (cmd.args and cmd.args .. ' - ' or '') .. (cmd.desc or C.locale:get('cmds', 'not_des'))
        })
      end
    end
    api:setMyCommands(a, lang)
  end
end

