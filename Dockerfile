FROM alpine AS build

RUN apk add luarocks5.1 libressl-dev gcc musl-dev lua5.1-dev \
  && luarocks-5.1 install luasec \
  && mkdir -p /dist/usr/local \
  && mkdir -p /dist/usr/local/lib/lua && cp -rpv /usr/local/lib/lua/* /dist/usr/local/lib/lua \
  && mkdir -p /dist/usr/local/share/lua && cp -rpv /usr/local/share/lua/* /dist/usr/local/share/lua

COPY run.sh /dist
COPY . /dist/app

FROM alpine
VOLUME ["/app"]

RUN apk add libressl luajit \
  && ln -sf $(which luajit) /usr/bin/lua \
  && adduser -D -h /app user

COPY --from=build /dist /
ENTRYPOINT ["/run.sh"]

