return function(C, api, msg)
  local l = msg.from.language_code
  local owner = msg.from.id == C.config.owner
  local cmd = C.cmds[msg.cmd]
  msg.l = l

  if not cmd
  then api:send(msg, C.locale:get('error', 'inv_cmd', l))

  elseif type(cmd.run) ~= 'function'
  then api:send(msg, C.locale:get('error', 'cmd_run', l))

  elseif cmd.private and not owner
  then api:send(msg, C.locale:get('error', 'adm_cmd', l))
  else
    if cmd.useQArgs
    then msg.args = api.parseArgs(api.unparseArgs(msg.args))
    end
    msg.loc = C.locale:get('cmds', msg.cmd, l)
    local suc, err = pcall(cmd.run, C, msg, owner)
    if not suc then
      print(err)
      api:forward(C.config.owner, msg, msg.message_id, false)
      api:send(C.config.owner, err)
      api:reply(msg, C.locale:get('error', 'not_suc', l))
    end
  end
end

