return {
  private = true,
  run = function(C, msg)
    local file = io.popen(C.api.unparseArgs(msg.args))
    if file then
      local r = file:read '*a'
      file:close()
      if #r == 0
      then r = '...'
      end
      C.api:reply(msg, r)
    else return C.api:reply(msg, 'error')
    end
  end
}

