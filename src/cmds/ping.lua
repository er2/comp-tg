return {
  run = function(C, msg)
    local t = os.time()
    local ps, ls = t - msg.date, t - C.loaded
    local lm = ls / 60
    local lh = lm / 60
    local ld = lh / 24
    C.api:send(msg, msg.loc.pat:format(ps, ld, lh, lm, ls))
  end
}

