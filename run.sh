#!/bin/sh

chown user:user -R /app 2> /dev/null

cd /app
su user -c "lua init.lua"

