class 'Locale' {
  __newindex = function() end,
  list = {
    'en',
    'ru',
    'ar',
  },
  main = 'en',

  __init = function(this)
    local json = require 'etc.json'
    for i = 1, #this.list do
      local n = this.list[i]
      local f = io.open('src/locales/'.. n ..'.json')
      this[n] = json.decode(f:read 'a')
    end
  end,

  function(this, C)
    C.locale = this
  end,

  get = function(this, category, key, lang)
    assert(category, 'Category not provided')
    assert(key, 'Key not provided')
    lang = lang or this.main
    local v = (this[lang] or {})[category]
    if not v
    then return this[this.main][category][key] or ''
    else return v[key] or ''
    end
  end,
}

return new 'Locale'

