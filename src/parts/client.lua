require 'etc.api'

return function(C)
  C.api = new 'API' {
    norun = true,
  }
  print 'Client initialization...'
  C.api._ev = function(_, t, i, name, ...)
    return C:_ev(t, i, name, C.api, ...)
  end

  C:load 'events'

  C.api:login(C.config.token, function()
    print('Logged on as @'.. C.api.info.username)
    C.config.token = nil
    C.api:emit 'ready'
  end)

  local offset = 0
  C.api.runs = true
  C:on('ready', function()
    while C.api.runs do
      C:emit 'tick'
      offset = C.api:recvUpdate(1, offset, 0)
    end
  end)
end

