return function(C, api, q)
  api.inline:answer(q.id, {
    api.inline.result('photo', '1',
      'https://cdn.discordapp.com/attachments/871474214270554124/876506659311202395/unknown.png'
    ):thumb 'https://cdn.discordapp.com/attachments/871474214270554124/876506659311202395/unknown.png',
    api.inline.result('photo', '2',
      'https://cdn.discordapp.com/attachments/710425649449402408/1002915343116795914/unknown.png'
    ):thumb 'https://cdn.discordapp.com/attachments/710425649449402408/1002915343116795914/unknown.png',
  })
end
