local config = require 'config'
require 'etc.events'
require 'class'

class 'Core' : inherits 'EventsThis' {
  config = config,
  loaded = 0,
  cmds = {},

  function(this)
    this:super()
    this:load 'parts'
    utf8 = require 'etc.utf8'
    require 'etc.utf8data'
    utf8.config = {
      conversion = {
        uc_lc = utf8_uc_lc,
        lc_uc = utf8_lc_uc
      }
    }
    utf8:init()
    print 'Done!'
    this:emit 'ready'
  end,

  stop = function(this)
    this.api:destroy()
    print 'Stopped'
    print('Uptime: ' .. (os.time() - self.loaded) .. ' seconds')
  end,

  load = function(this, what)
    local c = config[what]
    for i = 1, #c do
      local v = c[i]
      print('Loading ' .. what:sub(0, -2) ..' (' .. i ..' / '.. #c ..') ' .. v ..'...')

      -- Lint?
      local e, a = pcall(require, 'src.' .. what ..'.'.. v)
      print(e, a)

      if e then
        if what == 'events'
        then this.api:on(v, a)

        elseif what == 'cmds'
        then this.cmds[v] = a

        elseif what == 'parts'
        then a(this)
        end

      else print 'fail'
      end
    end
    print('Loaded '.. #c ..' '.. what)
    this.loaded = os.time()
  end,
}

local core = new 'Core' ()

